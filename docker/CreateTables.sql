CREATE DATABASE IF NOT EXISTS `ticker` ;

USE ticker;

CREATE TABLE IF NOT EXISTS `benutzer`(
  id int auto_increment NOT NULL,
  benutzername varchar(100) NOT NULL,

  PRIMARY KEY (id)
);