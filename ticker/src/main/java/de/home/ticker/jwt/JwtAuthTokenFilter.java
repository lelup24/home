package de.home.ticker.jwt;

import de.home.ticker.benutzer.BenutzerService;
import de.home.ticker.data.jooq.tables.daos.BenutzerDao;
import de.home.ticker.data.jooq.tables.pojos.Benutzer;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static de.home.ticker.data.jooq.tables.Benutzer.BENUTZER;


@Component
public class JwtAuthTokenFilter extends OncePerRequestFilter {

    @Value("${SECRET}")
    private String secret;

    private static final Logger logger = LoggerFactory.getLogger(JwtAuthTokenFilter.class);

    @Autowired
    private BenutzerDao benutzerDao;
    @Autowired
    private BenutzerService userDetailsService;




    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        try {

            String jwt = getJwt(request);


            if (jwt != null && validateJwtToken(jwt)) {

                String benutzername = getUserNameFromJwtToken(jwt);

                Benutzer benutzer = benutzerDao.fetchOne(BENUTZER.BENUTZERNAME, benutzername);

                if (benutzer == null) {
                    Benutzer neuerBenutzer = new Benutzer();
                    neuerBenutzer.setBenutzername(benutzername);
                    benutzerDao.insert(neuerBenutzer);
                }

                UserDetails userDetails = userDetailsService.loadUserByUsername(benutzername);

                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                        userDetails, null, userDetails.getAuthorities());
                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

                SecurityContextHolder.getContext().setAuthentication(authentication);


            }
        } catch (Exception e) {
            logger.error("Can NOT set user authentication -> Message: {e}", e);
        }

        filterChain.doFilter(request, response);
    }

    private String getJwt(HttpServletRequest request) {
        String authHeader = request.getHeader("Authorization");

        if (authHeader != null && authHeader.startsWith("Bearer ")) {
            return authHeader.replace("Bearer ", "");
        }

        return null;
    }

    // Set this to private if only used here
    public String getUserNameFromJwtToken(String token) {
        return Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(token)
                .getBody().getSubject();
    }

    // Set this to private if only used here
    public boolean validateJwtToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(secret).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException e) {
            logger.error("Invalid JWT signature -> Message: {} ", e.getMessage());
        } catch (MalformedJwtException e) {
            logger.error("Invalid JWT token -> Message: {}", e.getMessage());
        } catch (ExpiredJwtException e) {
            logger.error("Expired JWT token -> Message: Laufzeit des Tokens abgelaufen.");
        } catch (UnsupportedJwtException e) {
            logger.error("Unsupported JWT token -> Message: {}", e.getMessage());
        } catch (IllegalArgumentException e) {
            logger.error("JWT claims string is empty -> Message: {}", e.getMessage());
        }
        return false;
    }
}

