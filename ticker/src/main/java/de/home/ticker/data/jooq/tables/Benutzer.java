/*
 * This file is generated by jOOQ.
 */
package de.home.ticker.data.jooq.tables;


import de.home.ticker.data.jooq.Indexes;
import de.home.ticker.data.jooq.Keys;
import de.home.ticker.data.jooq.Ticker;
import de.home.ticker.data.jooq.tables.records.BenutzerRecord;

import java.util.Arrays;
import java.util.List;

import javax.annotation.processing.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Identity;
import org.jooq.Index;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Row3;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.4"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Benutzer extends TableImpl<BenutzerRecord> {

    private static final long serialVersionUID = -297261180;

    /**
     * The reference instance of <code>ticker.benutzer</code>
     */
    public static final Benutzer BENUTZER = new Benutzer();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<BenutzerRecord> getRecordType() {
        return BenutzerRecord.class;
    }

    /**
     * The column <code>ticker.benutzer.id</code>.
     */
    public final TableField<BenutzerRecord, Integer> ID = createField(DSL.name("id"), org.jooq.impl.SQLDataType.INTEGER.nullable(false).identity(true), this, "");

    /**
     * The column <code>ticker.benutzer.benutzername</code>.
     */
    public final TableField<BenutzerRecord, String> BENUTZERNAME = createField(DSL.name("benutzername"), org.jooq.impl.SQLDataType.VARCHAR(100).nullable(false), this, "");

    /**
     * The column <code>ticker.benutzer.passwort</code>.
     */
    public final TableField<BenutzerRecord, String> PASSWORT = createField(DSL.name("passwort"), org.jooq.impl.SQLDataType.VARCHAR(100), this, "");

    /**
     * Create a <code>ticker.benutzer</code> table reference
     */
    public Benutzer() {
        this(DSL.name("benutzer"), null);
    }

    /**
     * Create an aliased <code>ticker.benutzer</code> table reference
     */
    public Benutzer(String alias) {
        this(DSL.name(alias), BENUTZER);
    }

    /**
     * Create an aliased <code>ticker.benutzer</code> table reference
     */
    public Benutzer(Name alias) {
        this(alias, BENUTZER);
    }

    private Benutzer(Name alias, Table<BenutzerRecord> aliased) {
        this(alias, aliased, null);
    }

    private Benutzer(Name alias, Table<BenutzerRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""));
    }

    public <O extends Record> Benutzer(Table<O> child, ForeignKey<O, BenutzerRecord> key) {
        super(child, key, BENUTZER);
    }

    @Override
    public Schema getSchema() {
        return Ticker.TICKER;
    }

    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.BENUTZER_PRIMARY);
    }

    @Override
    public Identity<BenutzerRecord, Integer> getIdentity() {
        return Keys.IDENTITY_BENUTZER;
    }

    @Override
    public UniqueKey<BenutzerRecord> getPrimaryKey() {
        return Keys.KEY_BENUTZER_PRIMARY;
    }

    @Override
    public List<UniqueKey<BenutzerRecord>> getKeys() {
        return Arrays.<UniqueKey<BenutzerRecord>>asList(Keys.KEY_BENUTZER_PRIMARY);
    }

    @Override
    public Benutzer as(String alias) {
        return new Benutzer(DSL.name(alias), this);
    }

    @Override
    public Benutzer as(Name alias) {
        return new Benutzer(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public Benutzer rename(String name) {
        return new Benutzer(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public Benutzer rename(Name name) {
        return new Benutzer(name, null);
    }

    // -------------------------------------------------------------------------
    // Row3 type methods
    // -------------------------------------------------------------------------

    @Override
    public Row3<Integer, String, String> fieldsRow() {
        return (Row3) super.fieldsRow();
    }
}
