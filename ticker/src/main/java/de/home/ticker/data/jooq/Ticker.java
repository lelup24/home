/*
 * This file is generated by jOOQ.
 */
package de.home.ticker.data.jooq;


import de.home.ticker.data.jooq.tables.Benutzer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.processing.Generated;

import org.jooq.Catalog;
import org.jooq.Table;
import org.jooq.impl.SchemaImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.4"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Ticker extends SchemaImpl {

    private static final long serialVersionUID = 1398421621;

    /**
     * The reference instance of <code>ticker</code>
     */
    public static final Ticker TICKER = new Ticker();

    /**
     * The table <code>ticker.benutzer</code>.
     */
    public final Benutzer BENUTZER = de.home.ticker.data.jooq.tables.Benutzer.BENUTZER;

    /**
     * No further instances allowed
     */
    private Ticker() {
        super("ticker", null);
    }


    @Override
    public Catalog getCatalog() {
        return DefaultCatalog.DEFAULT_CATALOG;
    }

    @Override
    public final List<Table<?>> getTables() {
        List result = new ArrayList();
        result.addAll(getTables0());
        return result;
    }

    private final List<Table<?>> getTables0() {
        return Arrays.<Table<?>>asList(
            Benutzer.BENUTZER);
    }
}
