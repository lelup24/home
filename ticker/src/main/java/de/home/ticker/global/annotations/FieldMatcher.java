package de.home.ticker.global.annotations;

import de.home.ticker.global.validators.FieldMatchValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = FieldMatchValidator.class)
@Documented
public @interface FieldMatcher {
    String message() default "Felder müssen übereinstimmen";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String feld();
    String passendesFeld();

    @Target({ ElementType.TYPE })
    @Retention(RetentionPolicy.RUNTIME)
    @interface List {
        FieldMatcher[] value();
    }
}

