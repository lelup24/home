package de.home.ticker.global.validators;

import de.home.ticker.global.annotations.FieldMatcher;
import org.springframework.beans.BeanWrapperImpl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


public class FieldMatchValidator implements ConstraintValidator<FieldMatcher, Object> {

    private String erstesFeld;
    private String zweitesFeld;
    private String nachricht;

    @Override
    public void initialize(final FieldMatcher constraintAnnotation) {
        erstesFeld = constraintAnnotation.feld();
        zweitesFeld = constraintAnnotation.passendesFeld();
        nachricht = constraintAnnotation.message();
    }

    @Override
    public boolean isValid(final Object value, final ConstraintValidatorContext context) {
        boolean valid = true;
        try
        {
            final Object firstObj = new BeanWrapperImpl(value).getPropertyValue(erstesFeld);
            final Object secondObj = new BeanWrapperImpl(value).getPropertyValue(zweitesFeld);

            valid =  firstObj == null && secondObj == null || firstObj != null && firstObj.equals(secondObj);
        }
        catch (final Exception ignore)
        {
            // ignore
        }

        if (!valid){
            context.buildConstraintViolationWithTemplate(nachricht)
                    .addPropertyNode(erstesFeld)
                    .addConstraintViolation()
                    .disableDefaultConstraintViolation();
        }

        return valid;
    }
}
