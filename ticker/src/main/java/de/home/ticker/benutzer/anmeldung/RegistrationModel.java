package de.home.ticker.benutzer.anmeldung;

import de.home.ticker.global.annotations.FieldMatcher;

@FieldMatcher(feld = "passwort", passendesFeld = "passwortWiedderholung", message = "Müssen übereinstimmen")
public class RegistrationModel {

    private String benutzername;
    private String passwort;
    private String passwortWiederholung;

    public String getBenutzername() {
        return benutzername;
    }

    public void setBenutzername(String benutzername) {
        this.benutzername = benutzername;
    }

    public String getPasswort() {
        return passwort;
    }

    public void setPasswort(String passwort) {
        this.passwort = passwort;
    }

    public String getPasswortWiederholung() {
        return passwortWiederholung;
    }

    public void setPasswortWiederholung(String passwortWiederholung) {
        this.passwortWiederholung = passwortWiederholung;
    }
}
