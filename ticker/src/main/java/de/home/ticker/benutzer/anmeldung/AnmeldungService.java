package de.home.ticker.benutzer.anmeldung;

import de.home.ticker.jwt.JwtAuthTokenProvider;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;



@Service
public class AnmeldungService {

    private DaoAuthenticationProvider daoAuthenticationProvider;
    private JwtAuthTokenProvider jwtProvider;

    public AnmeldungService(DaoAuthenticationProvider daoAuthenticationProvider, JwtAuthTokenProvider jwtProvider) {
        this.daoAuthenticationProvider = daoAuthenticationProvider;
        this.jwtProvider = jwtProvider;
    }

    public TokenResponse anmelden(final AnmeldungModel model) {

        Authentication authentication = daoAuthenticationProvider
                .authenticate(new UsernamePasswordAuthenticationToken(model.getBenutzername(), model.getPasswort()));

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String authToken = jwtProvider.generateJwtToken(authentication);
        return new TokenResponse(authToken);
    }

    public Optional<TokenResponse> anmeldenRemote(AnmeldungModel model) {
        final RestTemplate restTemplate = new RestTemplate();

        ResponseEntity<TokenResponse> response = restTemplate.exchange(
                "http://localhost:8083/api/anmelden",
                HttpMethod.POST,
                new HttpEntity<>(model),
                TokenResponse.class);
        return Optional.ofNullable(response.getBody());
    }

}

