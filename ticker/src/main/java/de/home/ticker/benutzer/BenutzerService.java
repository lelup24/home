package de.home.ticker.benutzer;

import de.home.ticker.data.jooq.tables.daos.BenutzerDao;
import de.home.ticker.data.jooq.tables.pojos.Benutzer;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static de.home.ticker.data.jooq.tables.Benutzer.BENUTZER;

@Service
public class BenutzerService implements UserDetailsService {

    private final BenutzerDao benutzerDao;

    public BenutzerService(BenutzerDao benutzerDao) {
        this.benutzerDao = benutzerDao;
    }


    @Override
    public UserDetails loadUserByUsername(String benutzername) throws UsernameNotFoundException {
        final Benutzer benutzer = benutzerDao.fetchOne(BENUTZER.BENUTZERNAME, benutzername);
        if (benutzer == null) {
            throw new UsernameNotFoundException("Kein Benutzer mit diem Namen " + benutzername + " gefunden.");
        }
        return new User(benutzer.getBenutzername(), benutzer.getPasswort(), mapAuthority(Collections.singletonList("admmin")));
    }


    private Collection<GrantedAuthority> mapAuthority(List<String> rollen) {
        return rollen.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());
    }
}
