package de.home.ticker.benutzer;

import de.home.ticker.benutzer.anmeldung.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RequestMapping("/api/benutzer")
@RestController
public class BenutzerController {

    private final AnmeldungService anmeldungService;
    private final RegistrationService registrationService;

    public BenutzerController(AnmeldungService anmeldungService, RegistrationService registrationService) {
        this.anmeldungService = anmeldungService;
        this.registrationService = registrationService;
    }

    @PostMapping(value = "/anmeldung", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TokenResponse> anmelden(@RequestBody final AnmeldungModel model) {
        return new ResponseEntity<>(anmeldungService.anmelden(model), HttpStatus.OK);
    }

    @PostMapping(value = "/registration")
    public ResponseEntity<Void> registrieren(@Valid @RequestBody final RegistrationModel model) {
    System.out.println(model.getBenutzername());
        this.registrationService.registrieren(model);
        return ResponseEntity.noContent().build();
    }
}
