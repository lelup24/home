package de.home.ticker.benutzer.anmeldung;


import de.home.ticker.data.jooq.tables.daos.BenutzerDao;
import de.home.ticker.data.jooq.tables.pojos.Benutzer;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import static de.home.ticker.data.jooq.tables.Benutzer.BENUTZER;

@Service
public class RegistrationService {

    private final BenutzerDao benutzerDao;
    private final BCryptPasswordEncoder passwordEncoder;

    public RegistrationService(BenutzerDao benutzerDao, BCryptPasswordEncoder passwordEncoder) {
        this.benutzerDao = benutzerDao;
        this.passwordEncoder = passwordEncoder;
    }

    public void registrieren(final RegistrationModel model) {

        final Benutzer benutzer = benutzerDao.fetchOne(BENUTZER.BENUTZERNAME, model.getBenutzername());
        if (benutzer != null) {
            throw new RuntimeException("Ein Benutzer mit diesem Namen existiert bereits");
        }

        final String encodedPasswort = passwordEncoder.encode(model.getPasswort());

        Benutzer neuerBenutzer = new Benutzer();
        neuerBenutzer.setBenutzername(model.getBenutzername());
        neuerBenutzer.setPasswort(encodedPasswort);

        benutzerDao.insert(neuerBenutzer);
    }
}
