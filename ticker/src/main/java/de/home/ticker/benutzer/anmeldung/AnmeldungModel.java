package de.home.ticker.benutzer.anmeldung;

public class AnmeldungModel {

    private String benutzername;
    private String passwort;

    public String getBenutzername() {
        return benutzername;
    }

    public AnmeldungModel setBenutzername(String benutzername) {
        this.benutzername = benutzername;
        return this;
    }
    public String getPasswort() {
        return passwort;
    }

    public AnmeldungModel setPasswort(String passwort) {
        this.passwort = passwort;
        return this;
    }



}
