package de.home.ticker.benutzer.anmeldung;


public class TokenResponse {

    private String authToken;

    public TokenResponse() {}

    public TokenResponse(String authToken) {
        this.authToken = authToken;
    }

    public String getAuthToken() {
        return authToken;
    }

    public TokenResponse setAuthToken(String authToken) {
        this.authToken = authToken;
        return this;
    }



}

