package de.home.ticker.configs;


import org.jooq.SQLDialect;
import org.jooq.conf.RenderNameStyle;
import org.jooq.conf.Settings;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JooqConfig {

    @Bean
    Settings jooqSettings() {
        return new Settings()
                .withParseDialect(SQLDialect.MYSQL).withRenderNameStyle(RenderNameStyle.AS_IS);
    }


}

