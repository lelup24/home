package de.home.ticker.configs;

import de.home.ticker.benutzer.BenutzerService;
import de.home.ticker.jwt.JwtAuthEntryPoint;
import de.home.ticker.jwt.JwtAuthTokenFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(
        prePostEnabled = true
)
public class SecurityConfig extends WebSecurityConfigurerAdapter  {

    private final BenutzerService benutzerService;
    private final JwtAuthEntryPoint authHandler;
    private final JwtAuthTokenFilter authTokenFilter;

    public SecurityConfig(BenutzerService benutzerService, JwtAuthEntryPoint authHandler, JwtAuthTokenFilter authTokenFilter) {
        this.benutzerService = benutzerService;
        this.authHandler = authHandler;
        this.authTokenFilter = authTokenFilter;
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
       return new WebMvcConfigurer() {
         @Override
         public void addCorsMappings(CorsRegistry registry) {
            registry.addMapping("**")
            .allowedMethods("*")
            .allowedOrigins("*")
            .maxAge(3600);
          }
        };
      }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider auth = new DaoAuthenticationProvider();
        auth.setUserDetailsService(benutzerService);
        auth.setPasswordEncoder(passwordEncoder());
        return auth;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .cors().and()
            .csrf().disable()
            .authorizeRequests()
            .antMatchers("/ws/**").permitAll()
            .antMatchers("/api/benutzer/anmeldung").permitAll()
            .and()
            .exceptionHandling().authenticationEntryPoint(authHandler).and()
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http.addFilterBefore(authTokenFilter, UsernamePasswordAuthenticationFilter.class);
    }
}
