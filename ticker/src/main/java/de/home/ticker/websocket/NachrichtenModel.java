package de.home.ticker.websocket;

public class NachrichtenModel {

  private String absender;
  private String adressat;
  private String nachricht;
  private NachrichtenTyp nachrichtenTyp;

  public NachrichtenModel(String absender, String nachricht, NachrichtenTyp nachrichtenTyp) {
    this.absender = absender;
    this.nachricht = nachricht;
    this.nachrichtenTyp = nachrichtenTyp;
  }

    public String getAbsender() {
        return absender;
    }

    public NachrichtenModel setAbsender(String absender) {
        this.absender = absender;
        return this;
    }

    public String getAdressat() {
        return adressat;
    }

    public NachrichtenModel setAdressat(String adressat) {
        this.adressat = adressat;
        return this;
    }

    public String getNachricht() {
        return nachricht;
    }

    public NachrichtenModel setNachricht(String nachricht) {
        this.nachricht = nachricht;
        return this;
    }

    public NachrichtenTyp getNachrichtenTyp() {
        return nachrichtenTyp;
    }

    public NachrichtenModel setNachrichtenTyp(NachrichtenTyp nachrichtenTyp) {
        this.nachrichtenTyp = nachrichtenTyp;
        return this;
    }
}
