package de.home.ticker.websocket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class WebSocketListener {

    private static final Logger logger = LoggerFactory.getLogger(WebSocketListener.class);

    @Autowired
    private SimpMessageSendingOperations messagingTemplate;

    @EventListener
    public void handleWebSocketConnectListener(SessionConnectedEvent event) {
        logger.info("Received a new web socket connection");

        Principal principal = event.getUser();
        if (principal != null) {
            logger.info("User Connected : " + principal.getName());
            final String zeit = new SimpleDateFormat("HH:mm:ss").format(new Date());
            NachrichtenOutputModel chatMessage =
                    new NachrichtenOutputModel("System", principal.getName() + " ist dem Chat beigeteten", zeit, NachrichtenTyp.JOIN);
            messagingTemplate.convertAndSend("/topic/nachrichten", chatMessage);
        }

    }


    @EventListener
    public void handleWebSocketDisconnectListener(SessionDisconnectEvent event) {

        Principal principal = event.getUser();
        if(principal != null) {
            logger.info("User Disconnected : " + principal.getName());
            final String zeit = new SimpleDateFormat("HH:mm:ss").format(new Date());
            NachrichtenOutputModel chatMessage = new NachrichtenOutputModel("System", principal.getName() + " hat den Chat verlassen", zeit, NachrichtenTyp.LEAVE);

            messagingTemplate.convertAndSend("/topic/nachrichten", chatMessage);
        }
    }
}
