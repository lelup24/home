package de.home.ticker.websocket;

import de.home.ticker.benutzer.BenutzerService;
import de.home.ticker.jwt.JwtAuthTokenFilter;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.config.ChannelRegistration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.messaging.support.MessageHeaderAccessor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

import java.util.Objects;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

    private final JwtAuthTokenFilter jwtAuthTokenFilter;
    private final BenutzerService benutzerService;

    public WebSocketConfig(JwtAuthTokenFilter jwtAuthTokenFilter, BenutzerService benutzerService) {
        this.jwtAuthTokenFilter = jwtAuthTokenFilter;
        this.benutzerService = benutzerService;
    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        registry.enableSimpleBroker("/topic", "/queue");
        registry.setApplicationDestinationPrefixes("/app");
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
    registry
        .addEndpoint("/ws")
        .setAllowedOrigins("*")
        .withSockJS();
    }

    @Override
    public void configureClientInboundChannel(ChannelRegistration registration) {
    registration.interceptors(
        new ChannelInterceptor() {
          @Override
          public Message<?> preSend(Message<?> message, MessageChannel channel) {
            StompHeaderAccessor accessor =
                MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);

            String token = "";
              assert accessor != null;
              if (accessor.getFirstNativeHeader("AuthToken") != null && Objects.requireNonNull(accessor.getFirstNativeHeader("AuthToken")).length() > 0) {
              token = Objects.requireNonNull(accessor.getNativeHeader("AuthToken")).get(0);
            }

            if (StompCommand.CONNECT.equals(accessor.getCommand())) {

              if (jwtAuthTokenFilter.validateJwtToken(token)) {

                final String benuzername = jwtAuthTokenFilter.getUserNameFromJwtToken(token);

                UserDetails userDetails = benutzerService.loadUserByUsername(benuzername);

                UsernamePasswordAuthenticationToken authentication =
                    new UsernamePasswordAuthenticationToken(
                        userDetails, null, userDetails.getAuthorities());

                accessor.setUser(authentication);
              }
            }
            return message;
          }
        });
    }


}
