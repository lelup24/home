package de.home.ticker.websocket;

public class NachrichtenOutputModel {

    private String absender;
    private String nachricht;
    private String zeit;
    private NachrichtenTyp nachrichtenTyp;

    public NachrichtenOutputModel(String absender, String nachricht, String zeit, NachrichtenTyp nachrichtenTyp) {
        this.absender = absender;
        this.nachricht = nachricht;
        this.zeit = zeit;
        this.nachrichtenTyp = nachrichtenTyp;
    }

    public String getAbsender() {
        return absender;
    }

    public NachrichtenOutputModel setAbsender(String absender) {
        this.absender = absender;
        return this;
    }

    public String getNachricht() {
        return nachricht;
    }

    public NachrichtenOutputModel setNachricht(String nachricht) {
        this.nachricht = nachricht;
        return this;
    }

    public String getZeit() {
        return zeit;
    }

    public NachrichtenOutputModel setZeit(String zeit) {
        this.zeit = zeit;
        return this;
    }

    public NachrichtenTyp getNachrichtenTyp() {
        return nachrichtenTyp;
    }

    public NachrichtenOutputModel setNachrichtenTyp(NachrichtenTyp nachrichtenTyp) {
        this.nachrichtenTyp = nachrichtenTyp;
        return this;
    }
}
