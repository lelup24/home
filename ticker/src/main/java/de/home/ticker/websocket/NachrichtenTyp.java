package de.home.ticker.websocket;

public enum NachrichtenTyp {
    CHAT,
    PRIVATE,
    JOIN,
    LEAVE
}
