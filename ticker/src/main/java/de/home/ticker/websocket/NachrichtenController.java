package de.home.ticker.websocket;

import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;

import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Date;


@Controller
public class NachrichtenController {

    private SimpMessagingTemplate simpMessagingTemplate;

    public NachrichtenController(SimpMessagingTemplate simpMessagingTemplate) {
        this.simpMessagingTemplate = simpMessagingTemplate;
    }


    @MessageMapping("/nachricht")
    @SendTo("/topic/nachrichten")
    public NachrichtenOutputModel getNachricht(final NachrichtenModel nachrichtenModel, Principal principal) {
        final String zeit = new SimpleDateFormat("HH:mm:ss").format(new Date());
        return new NachrichtenOutputModel(principal.getName(), nachrichtenModel.getNachricht(), zeit, nachrichtenModel.getNachrichtenTyp());
    }

    @MessageMapping("/private-nachricht")
    public void erfolg(@Payload final NachrichtenModel nachricht, Principal principal) {
        final String zeit = new SimpleDateFormat("HH:mm:ss").format(new Date());
        simpMessagingTemplate
                .convertAndSend("/user/" + nachricht.getAdressat() + "/queue/user",
                        new NachrichtenOutputModel(principal.getName(), nachricht.getNachricht(), zeit, nachricht.getNachrichtenTyp()));
    }

    @MessageExceptionHandler
    @SendToUser("/queue/errors")
    public String handleException(Throwable exception) {
        return exception.getMessage();
    }
}
