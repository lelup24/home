import {Component, OnInit} from '@angular/core';
import {Karte} from "./global/karte/karte.component";
import {Title} from "@angular/platform-browser";
import {AmpelStatus} from "./global/ampel/ampel.component";
import {TickerService} from "./ticker/ticker.service";
import {TokenStorageService} from "./Benutzer/token-storage.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
  private title = 'Sonnenschein Home System';
  public ampelStatus: AmpelStatus;
  public anmeldungOffen = false;
  public registrationOffen = false;

  karten: Karte[] = [
    { bild: 'amazon.png', url: 'https://www.amazon.de'},
    { bild: 'youtube.jpg', url: 'https://www.youtube.de'},
    { bild: 'twitch.jpg', url: 'https://www.twitch.com'},
    { bild: 'google.jpg', url: 'https://www.google.com'},
    { bild: 'webde.png', url: 'https://www.web.de'},
    { bild: 'weltde.jpg', url: 'https://www.welt.de'},
    { bild: 'iubh.jpg', url: 'https://care-fs.iubh.de'},
    { bild: 'gmx.png', url: 'https://www.gmx.de'},
  ];

  constructor(private titleService: Title, private ticker: TickerService, private tokenStorageService: TokenStorageService) {
  }

  ngOnInit(): void {
    this.titleService.setTitle(this.title);
    this.ticker.$ampelStatus.subscribe(res => this.ampelStatus = res);
  }

  public closeAnmeldung() {
    this.anmeldungOffen = false;
  }

  public closeRegistration() {
    this.registrationOffen = false;
  }

  get isToken() {
    return this.tokenStorageService.isToken();
  }

  get benutzername() {
    return this.tokenStorageService.getSubject();
  }

}
