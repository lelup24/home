import {Injectable} from "@angular/core";
import {Stomp} from "@stomp/stompjs";
import * as SockJS from 'sockjs-client';
import {BehaviorSubject} from "rxjs";
import {NachrichtenModel, NachrichtenTyp} from "../WebSocket/NachrichtenModel";
import {AmpelStatus} from "../global/ampel/ampel.component";
import {TokenStorageService} from "../Benutzer/token-storage.service";

@Injectable({
  providedIn: 'root'
})
export class TickerService {

  private stompClient: any;
  public $nachrichten: BehaviorSubject<NachrichtenModel[]> = new BehaviorSubject<NachrichtenModel[]>([]);
  private nachrichten: NachrichtenModel[] = [];
  public $ampelStatus: BehaviorSubject<AmpelStatus> = new BehaviorSubject<AmpelStatus>(AmpelStatus.GRUEN);

  constructor(public token: TokenStorageService) {
  }

  public connect() {
    let ws = new SockJS("http://192.168.8.101:8080/ws");

    this.stompClient = Stomp.over(() => {return ws});

    const _this = this;

    //this.stompClient.debug  = function() {};

    this.stompClient.connect({AuthToken: this.token.getToken()}, function (_frame) {
      _this.stompClient.subscribe('/topic/nachrichten', function (sdkEvent) {
        _this._onMessageReceived(sdkEvent);
      });

      _this.stompClient.subscribe('/user/queue/user', function (message) {
        _this._onMessageReceived(message);
      });

      _this.stompClient.subscribe('/user/queue/errors', function (message) {
          alert(message)
      });

      _this.stompClient.reconnect_delay = 2000;
    }, _this.errorCallBack);
  }


  disconnect() {
    if (this.stompClient !== null) {
      this.stompClient.disconnect();
    }
  }

  errorCallBack(error) {
    console.log("errorCallBack -> " + error);
    setTimeout(() => {
      this.connect();
    }, 5000);
  }

  send(nachricht: NachrichtenModel) {

    if (nachricht.nachricht.includes('@')) {
      const words  = nachricht.nachricht.split(" ");
      let adressat = '';
      for (let i = 0, n = words.length; i < n; i++) {
        if (words[i].charAt(0) === '@') {
          adressat = words[i].substr(1, words[i].length);
        }
      }
      nachricht.adressat = adressat;
      nachricht.nachrichtenTyp = NachrichtenTyp.PRIVATE;
    }

    if (nachricht.adressat === '' || nachricht.adressat === undefined) {
      nachricht.nachrichtenTyp = NachrichtenTyp.CHAT;
      this.stompClient.send("/app/nachricht", {}, JSON.stringify(nachricht));
    }
    {
      nachricht.nachrichtenTyp = NachrichtenTyp.PRIVATE;
      this.stompClient.send("/app/private-nachricht", {}, JSON.stringify(nachricht));
    }
  }

  _onMessageReceived(message) {
    this.handleMessage(message.body);
  }

  handleMessage(message){

    let msg: NachrichtenModel = JSON.parse(message);

    if (msg.nachricht.toLowerCase().includes('ampel:')) {
      const status: AmpelStatus = AmpelStatus[msg.nachricht.split(':')[1].toUpperCase()];
      this.$ampelStatus.next(status);
      return;
    }

    this.nachrichten = [...this.nachrichten, msg];
    if (this.nachrichten.length > 10) {
      this.nachrichten.shift();
    }
    this.$nachrichten.next(this.nachrichten);

  }


}
