import {NachrichtenModel} from "../WebSocket/NachrichtenModel";
import {Component, OnDestroy, OnInit} from "@angular/core";

import {TickerService} from "./ticker.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";


@Component({
  selector: 'app-ticker',
  templateUrl: './ticker.component.html',
  styleUrls: ['./ticker.component.sass']
})
export class TickerComponent implements OnInit, OnDestroy {

  public nachrichten: NachrichtenModel[] = [];
  public tickerForm: FormGroup;
  public isConnected = false;


  constructor(private tickerService: TickerService) {
  }

  ngOnInit(): void {
    this.tickerForm = TickerComponent.createTickerForm();
    this.tickerService.$nachrichten.subscribe(msgs => {
      this.nachrichten = msgs;
    });
  }

  public absenden() {
    this.tickerService.send(this.tickerForm.value);
    this.tickerService.$ampelStatus.subscribe(() => {
    });
    this.resetForm();

  }

  private resetForm() {
    this.tickerForm.markAsUntouched();
    this.tickerForm.markAsPristine();
    this.tickerForm.clearAsyncValidators();
    this.tickerForm.reset();
  }

  private static createTickerForm() {
    return new FormGroup({
      nachricht: new FormControl('', Validators.required),
    })
  }

  ngOnDestroy(): void {
    this.tickerService.disconnect();
    this.isConnected = false;
  }

  onConnect() {
    this.tickerService.connect();
    this.isConnected = true;
  }

  onDisconnect() {
    this.tickerService.disconnect();
    this.isConnected = false;
  }


}
