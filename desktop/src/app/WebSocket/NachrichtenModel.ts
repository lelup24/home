export enum NachrichtenTyp {
  CHAT = "CHAT",
  PRIVATE = "PRIVATE",
  JOIN = "JOIN",
  LEAVE = "LEAVE"
}

export interface NachrichtenModel {
  absender: string;
  adressat?: string;
  nachricht: string;
  zeit?: string;
  nachrichtenTyp: NachrichtenTyp;
}
