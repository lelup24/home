import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatCardModule} from "@angular/material/card";
import {KarteComponent} from './global/karte/karte.component';
import {TickerComponent} from './ticker/ticker.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ReversePipePipe} from './global/pipes/reverse-pipe.pipe';
import {SortPipe} from './global/pipes/sort.pipe';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";
import {MatListModule} from "@angular/material/list";
import {AmpelComponent} from './global/ampel/ampel.component';
import {AnmeldungComponent} from "./Benutzer/anmeldung/anmeldung.component";
import {HttpClientModule} from "@angular/common/http";
import {JwtModule} from "@auth0/angular-jwt";
import {RegistrationComponent} from "./Benutzer/registration/registration.component";
import {httpInterceptorProviders} from "./Benutzer/auth-interceptor.service";

export function tokenGetter() {
  return localStorage.getItem('AuthToken');
}


@NgModule({
  declarations: [
    AppComponent,
    KarteComponent,
    TickerComponent,
    ReversePipePipe,
    SortPipe,
    AmpelComponent,
    AnmeldungComponent,
    RegistrationComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatListModule,
    HttpClientModule,
    JwtModule.forRoot({config: {
      tokenGetter
      }})
  ],
  providers: [httpInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
