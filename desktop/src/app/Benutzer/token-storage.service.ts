import {Injectable} from '@angular/core';
import {JwtHelperService} from '@auth0/angular-jwt';

const BASE = 'AuthToken';

@Injectable({
    providedIn: 'root'
})
export class TokenStorageService {

    constructor(private jwt: JwtHelperService) {}

    public saveToken(token: string): void {
        window.localStorage.removeItem(BASE);
        window.localStorage.setItem(BASE, token);
    }

    public isToken(): boolean {
        const token = localStorage.getItem(BASE);
        return !this.jwt.isTokenExpired(token);
    }

    public getToken(): string {
        const token = localStorage.getItem(BASE);
        if (!this.jwt.isTokenExpired(token)) {
            return token;
        }
    }

    public getSubject(): string {
      const token = localStorage.getItem(BASE);
      const payload = this.jwt.decodeToken(token);
      return payload.sub;

    }



}
