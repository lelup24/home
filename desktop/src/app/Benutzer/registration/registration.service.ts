import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {RegistrationModel} from './registration.model';

const BASE = 'http://localhost:8080/api/benutzer/registration';

@Injectable({
    providedIn: 'root'
})
export class RegistrationService {


    constructor(private http: HttpClient) {}

    registrieren(value: RegistrationModel): Observable<void> {
        return this.http.post<void>(BASE, value);
    }

}
