import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {RegistrationService} from './registration.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.sass']
})
export class RegistrationComponent implements OnInit {
  @Input() open: boolean;
  @Output() closePopup = new EventEmitter();
  registrationForm: FormGroup;

  constructor(private registrationService: RegistrationService) { }

  ngOnInit() {
    this.createRegistrationForm();
  }

  onClose() {
    this.open = false;
    this.closePopup.emit(false);
  }

  createRegistrationForm() {
    this.registrationForm = new FormGroup({
      benutzername: new FormControl(null, Validators.compose([Validators.required])),
      passwort: new FormControl(null, Validators.compose([Validators.required])),
      passwortWiederholt: new FormControl(null, Validators.compose([Validators.required]))
    });
  }

  onCompleteForm() {
    this.registrationService.registrieren(this.registrationForm.value).subscribe(
      () => {
        this.onClose();
        alert('success');
      },
      (err) => {
        alert(err.error.message);
      }
    );
  }

}
