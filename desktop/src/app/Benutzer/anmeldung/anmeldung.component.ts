import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AnmeldungService} from './anmeldung.service';
import {TokenStorageService} from "../token-storage.service";

@Component({
  selector: 'app-anmeldung',
  templateUrl: './anmeldung.component.html',
  styleUrls: ['./anmeldung.component.sass']
})
export class AnmeldungComponent implements OnInit {
  @Input() open: boolean;
  @Output() closeAnmeldung = new EventEmitter();
  anmeldeFormGroup: FormGroup;

  constructor(private anmeldungService: AnmeldungService, private tokenStorage: TokenStorageService) { }



  ngOnInit() {
    this.createRegistrationForm();
  }

  onClose() {
    this.open = false;
    this.closeAnmeldung.emit(false);
  }

  createRegistrationForm() {
    this.anmeldeFormGroup = new FormGroup({
      benutzername: new FormControl('', Validators.compose([Validators.required])),
      passwort: new FormControl('', Validators.compose([Validators.required])),
    });
  }

  onCompleteForm() {
    this.anmeldungService.anmelden(this.anmeldeFormGroup.value).subscribe(
      (result: any) => {
        this.onClose();
        this.tokenStorage.saveToken(result.authToken);
      },
      (err) => {
        alert('Fehler:' + err.message);
      }
    );
  }

}
