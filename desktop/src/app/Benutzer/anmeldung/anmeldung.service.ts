import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AnmeldungModel} from './anmeldung.model';

const BASE_ANMELDUNG = 'http://192.168.8.101:8080/api/benutzer/anmeldung';

@Injectable({
    providedIn: 'root'
})
export class AnmeldungService {

    constructor(private http: HttpClient) {}

    public anmelden(value: AnmeldungModel): Observable<boolean> {
        return this.http.post<boolean>(BASE_ANMELDUNG, value);
    }
}
