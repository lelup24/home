import { Pipe, PipeTransform } from '@angular/core';
import {NachrichtenModel} from "../../WebSocket/NachrichtenModel";

@Pipe({
  name: 'sort'
})
export class SortPipe implements PipeTransform {

  transform(nachrichten: NachrichtenModel[]): NachrichtenModel[] {
    return [...nachrichten].sort((a, b) => b.zeit.localeCompare(a.zeit));
  }

}
