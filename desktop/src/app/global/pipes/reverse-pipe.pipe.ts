import { Pipe, PipeTransform } from '@angular/core';
import {NachrichtenModel} from "../../WebSocket/NachrichtenModel";

@Pipe({
  name: 'reversePipe'
})
export class ReversePipePipe implements PipeTransform {

  transform(nachrichten: NachrichtenModel[]): NachrichtenModel[] {
    return nachrichten.reverse();
  }

}
