import {Component, Input, OnInit} from '@angular/core';

export enum AmpelStatus {
  ROT = "rot",
  GELB = "gelb",
  GRUEN = "gruen"
}

@Component({
  selector: 'app-ampel',
  templateUrl: './ampel.component.html',
  styleUrls: ['./ampel.component.sass']
})
export class AmpelComponent implements OnInit {
  @Input()
  public status: AmpelStatus = AmpelStatus.ROT;

  constructor() { }

  ngOnInit(): void {
  }

}
