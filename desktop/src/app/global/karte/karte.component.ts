import {Component, Input, OnInit} from '@angular/core';
import {Router} from "@angular/router";

export interface Karte {
  bild: string,
  url: string
}

@Component({
  selector: 'app-karte',
  templateUrl: './karte.component.html',
  styleUrls: ['./karte.component.sass']
})
export class KarteComponent implements OnInit {
  @Input()
  bild: string;
  @Input()
  url: string;

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  public navigate() {
    window.open(this.url, '_blank');
  }

}
