const {
  app,
  BrowserWindow,
  Menu,
  MenuItem,
  session
} = require('electron');
const url = require("url");
const path = require("path");

let appWindow;

function initWindow() {
  appWindow = new BrowserWindow({
    webPreferences: {
      nodeIntegration: false
    },
    width: 1240,
    height: 1600,
    frame: false,
    autoHideMenuBar: true
  });

  session.defaultSession.webRequest.onHeadersReceived((details, callback) => {
    callback({
      responseHeaders: {
        ...details.responseHeaders,
        referrer: {
          policy: 'no-referrer-when-downgrade'
        },
      //   'Content-Security-Policy':
        //       //     ['default-src \'self\' http://youtube.com https:',
        //       //       'script-src \'self\' http://youtube.com https:',
        //       //       'frame-src \'self\' http://youtube.com https:',
        //       //       'video-src \'self\' http://youtube.com https:']
              }
    })
  });
  // Electron Build Path
  appWindow.loadURL(
    url.format({
      pathname: path.join(__dirname, `/dist/index.html`),
      protocol: "file:",
      slashes: true
    })
  );

  appWindow.setFullScreen(true);


  // Initialize the DevTools.
  //appWindow.webContents.openDevTools()

  appWindow.on('closed', function () {
    appWindow = null
  });


  appWindow.webContents.on('new-window', (event, url) => {
      event.preventDefault();
      const options = {
        webPreferences: {
          nodeIntegration: false,
          preload: './preload.js'
        },
        frame: true,
        modal: false,
        parent: appWindow,
        width: 800,
        height: 800,
        url: url,
        type: 'desktop',
        fullscreenable: true,
        autoHideMenuBar: true,
        x: 0,
        center: true,
        referrer: {
          policy: 'no-referrer-when-downgrade'
        },
      };
      const win = new BrowserWindow(options);
      //win.webContents.openDevTools();
      const menu = new Menu();

      menu.append(new MenuItem(
        { label: 'Zurück', click() { win.webContents.canGoBack() ? win.webContents.goBack() : '' } }));
      menu.append(new MenuItem(
        { label: 'Vorwärts', click() { win.webContents.canGoForward() ? win.webContents.goForward() : ''}  }));


      Menu.setApplicationMenu(menu);
      win.setMenu(menu);

      win.loadURL(url, {httpReferrer: 'no-referrer'});

  })


}
app.allowRendererProcessReuse = true;

app.on('ready', initWindow);

// Close when all windows are closed.
app.on('window-all-closed', function () {

  // On macOS specific close process
  if (process.platform !== 'darwin') {
    app.quit()
  }
});

app.on('activate', function () {
  if (appWindow === null) {
    initWindow()
  }
});

